# Guia de como subir archivos a un repositorio de Bitbucket.

## Crear el repositorio en Bitbucket.

Desde nuestra cuenta podemos ir a "repositories" y en la parte superior derecha pone crear repository. Cuando lo creamos le ponemos un nombre, ya ya tendriamos nuestro repositorio donde subiriamos nuestro proyecto.

## Preparar los directorios para el repositorio.

Yo , lo que he hecho ha sido crear una carpeta para guardar dentro el repositorio.
Si creamos la carpeta ,vamos hacia ella por comandos usando **cd**

```
C:\Users\Falopio>cd Desktop\"Sistemas informaticoos"
```

Una vez dentro voy a usar **git cone "link del repositorio"**

```
$ git clone https://bitbucket.org/erikpc/actividad2
```

Esto hará que en el derectorio clone el contenido del repositorio y lo prepare para subir el contenido al repositorio remoto.

## Subir contenido al repositorio remoto

Supongamos que hemos creado un txt de prueba para subirlo llamado "hello-world.txt".
Decidimos subirlo, haremos lo siguiente. Haremos un **git init** y despues un **git status** para comprobar que git esta en ese directorio

```
$ git init
$ git status
```

Cuando queramos subirlo haremos lo siguiente, **git add "nombre del archivo"** y vamos a comentarlo usando **git commit -m "aqui el comentario"**

```
$ git add hello-world.txt
$ git commit -m "este es un hello world para hacer una prueba"
```

Una vez hemos comentado lo que vamos a añadir al repositorio , toca subirlo.
Usaremos el comando **git push** colocaremos despues el origen y la rama donde lo vamos a subir.

En mi caso será lo siguiente.

```
$ git push origin main
```

Una vez usado el comando saldrá una ventana emergente si es la primera vez que lo usas , en esa ventana inicias sesion y ya subira los archivos al repositorio

### Fin

Espero que esta guia le haya servido y le haya ayudado a comenzar a usar Bitbucket. Que tenga un buen día.

<img src="Captura1.PNG">
